package protohackers

import (
	"bufio"
	"context"
	"fmt"
	"net"
	"regexp"
)

var nameRE = regexp.MustCompile(`[a-zA-Z0-9]{1, 32}`)

type State byte

const (
	Open  State = 0
	Close State = 1
	Text  State = 2
)

type Msg struct {
	Conn  *net.TCPConn
	Text  []byte
	State State
}

func sendOthers(c *net.TCPConn, users map[*net.TCPConn]string, msg string) {
	b := []byte(msg + "\n")
	for conn := range users {
		if conn != c {
			conn.Write(b)
		}
	}
}

func serve(ctx context.Context) func(*net.TCPConn) error {
	var users = make(map[*net.TCPConn]string)
	var msgs = make(chan Msg)

	go func() {
	SERVE:
		for {
			select {
			case <-ctx.Done():
				return
			case m := <-msgs:
				switch m.State {
				case Open:
					name := string(m.Text)
					if !nameRE.MatchString(name) {
						m.Conn.Write([]byte("invalid username\n"))
						m.Conn.Close()
						continue SERVE
					}
					users[m.Conn] = name
					sendOthers(m.Conn, users, fmt.Sprintf("* %s has entered the chat", name))
				case Close:
					if name, ok := users[m.Conn]; ok {
						sendOthers(m.Conn, users, fmt.Sprintf("* %s has left the chat", name))
						delete(users, m.Conn)
					}
				case Text:
					sendOthers(m.Conn, users, fmt.Sprintf("[%s] %s", users[m.Conn], m.Text))
				}

			}
		}
	}()

	return func(conn *net.TCPConn) error {
		conn.Write([]byte("welcome to budgetchat. enter name: 1-32 alphanumeric characters"))
		scanner := bufio.NewScanner(conn)
		scanner.Scan()
		msgs <- Msg{Conn: conn, Text: scanner.Bytes()}
		defer func() {
			conn.Close()
			msgs <- Msg{Conn: conn, State: Close}
		}()
		for scanner.Scan() {
			msgs <- Msg{Conn: conn, Text: scanner.Bytes()}
		}
		return nil
	}
}
