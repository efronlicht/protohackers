package protohackers

import (
	"io"
	"net"
)

func Echo(conn *net.TCPConn) error {
	buf := make([]byte, 4096)
	for {
		n, err := conn.Read(buf)
		if n > 0 {
			conn.Write(buf[:n])
		}
		switch err {
		case nil:
			continue
		case io.EOF:
			return nil
		default:
			return err
		}
	}
}
