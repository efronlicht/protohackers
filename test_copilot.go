package protohackers

import "time"

func daysInYear(t time.Time) int {
	if y := t.Year(); y%4 == 0 && (y%100 != 0 || y%400 == 0) {
		return 366
	}
	return 365
}
