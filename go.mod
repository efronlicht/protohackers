module gitlab.com/efronlicht/protohackers

go 1.19

require (
	github.com/tidwall/btree v1.6.0
	gitlab.com/efronlicht/estd v0.1.2
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/rs/zerolog v1.28.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.23.0 // indirect
	golang.org/x/exp v0.0.0-20221012211006-4de253d81b95 // indirect
	golang.org/x/sys v0.1.0 // indirect
)
