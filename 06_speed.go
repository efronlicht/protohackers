package protohackers

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"net"
	"os"
	"time"

	"github.com/tidwall/btree"
	"gitlab.com/efronlicht/estd/containers/set"
	"gitlab.com/efronlicht/estd/enve"
)

func panicf(format string, args ...any) { panic(fmt.Errorf(format, args...)) }

var be = binary.BigEndian

type ConnType byte

var (
	cameras            = make(map[*net.TCPConn]IAmCamera)
	dispatchersToRoads = make(map[*net.TCPConn]set.HSet[uint16])
	roadsToDispatchers = make(map[uint16]set.HSet[*net.TCPConn])
	seenHeartbeat      = make(map[*net.TCPConn]struct{})
	tickets            = make(map[string]map[uint32]bool)
	// plate/road -> offset -> timestamp
	snapshots = make(map[Shot]*btree.Map[uint16, uint32])
)

type Shot struct {
	Plate string
	Road  uint16
}

func str(b []byte) (s string, remaining []byte, err error) {
	if len(b) == 0 {
		return "", nil, errors.New("empty data")
	}
	n := int(b[0])
	b = b[1:]
	if len(b) < n {
		return "", nil, fmt.Errorf("expected at least %d bytes, got %d", n, len(b))
	}
	return string(b[:n]), b[n:], nil
}

const (
	_Error         = 0x10
	_Plate         = 0x20
	_Ticket        = 0x21
	_Hearbeat      = 0x41
	_IAmDispatcher = 0x81
)

var Names = [255]string{
	_Error:         "0x10 error",
	_Plate:         "0x20 plate",
	_Ticket:        "0x21 ticket",
	_Hearbeat:      "0x41 heartbeat",
	_IAmDispatcher: "0x81 i am dispatcher",
}

type (
	WantHeartbeat struct {
		Interval time.Duration
	}
	Ticket struct {
		Plate                     string
		Road, Mile1, Mile2, Speed uint16
		Timestamp1, Timestamp2    uint32
	}
	Plate struct {
		Plate     string
		Timestamp uint32
	}
	Error     string
	Heartbeat struct{}
	IAmCamera struct{ Road, Mile, Limit uint16 }
)

func logDst() io.Writer {
	if enve.BoolOr("LOG_ENABLED", true) {
		return os.Stderr
	}
	return io.Discard
}

func handle(conn *net.TCPConn) (err error) {
	logger := log.New(logDst(), fmt.Sprintf("%s %s\t", conn.LocalAddr(), conn.RemoteAddr()), log.LstdFlags|log.Lshortfile)

	defer func() { //  close connection and send error if any
		if err != nil {
			logger.Print("sending err", err)

			s := err.Error()
			// truncate packet to 255 bytes
			n := min(len(s), 255)

			// When the client does something that this protocol specification declares "an error", the
			// server must send the client an appropriate Error message and immediately disconnect that client.
			b := append([]byte{0x10, byte(n)}, s[:n]...)
			if _, err := conn.Write(b); err != nil {
				logger.Printf("write: %v", err)
			}
		} else {
			logger.Print("ok")
		}
		if err := conn.Close(); err != nil {
			logger.Printf("close: err: %v", err)
		} else {
			logger.Printf("close: ok")
		}
	}()
	buf := make([]byte, 1024*8)
	for {
		n, err := conn.Read(buf)
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		type_, data := buf[0], buf[1:n]

		switch type_ {
		case 0x20: // Plate
			if _, ok := dispatchersToRoads[conn]; ok {
				return fmt.Errorf("plate from dispatcher")
			}
			cam, ok := cameras[conn]
			if !ok {
				return fmt.Errorf("expected an IAmCamera before a Plate")
			}
			plate, data, err := str(data)
			if err != nil {
				return fmt.Errorf("plate: bad string %v", err)
			}
			k := Shot{plate, cam.Road}
			if snapshots[k] == nil {
				snapshots[k] = new(btree.Map[uint16, uint32])
			}
			snapshots[k].Set(cam.Mile, be.Uint32(data))
			lastPosRaw, lastTimestampRaw, _ := snapshots[k].Min() // must exist; just added
			lastPos, lastTimestamp := float64(lastPosRaw), float64(lastTimestampRaw)

			// check for tickets
			snapshots[k].Ascend(0, func(rawPos uint16, rawTS uint32) bool {
				pos, timestamp := float64(rawPos), float64(rawTS)

				// this defer executes at the end of each iteration of the loop,
				// not at the end of handle()
				defer func() { lastPos, lastTimestamp = pos, timestamp }()

				dist := pos - lastPos
				elapsedHours := (timestamp - lastTimestamp) / 3600.0
				// must ticket speeders who exceed the limit by 0.5MPH or more; we use 0.3 to avoid rounding errors
				mph := math.Abs(dist / elapsedHours)
				if (mph - 0.3) < float64(cam.Limit) { // no ticket
					return true
				}
				// send a ticket for each day this covers, so long as a ticket for this plate/day hasn't already been sent
				const secsPerDay = 24 * 60 * 60
				for day, endDay := uint32(math.Floor(lastTimestamp/secsPerDay)), uint32(math.Floor(timestamp/secsPerDay)); day <= endDay; day++ {
					if tickets[plate] == nil {
						tickets[plate] = make(map[uint32]bool)
					}
					if !tickets[plate][day] {
						ticket := struct {
							Plate                     string
							Road, Mile1, Mile2, Speed uint16
							Timestamp1, Timestamp2    uint32
						}{
							Plate:      plate,
							Road:       cam.Road,
							Mile1:      uint16(lastPos),
							Mile2:      uint16(pos),
							Speed:      uint16(mph),
							Timestamp1: uint32(lastTimestamp),
							Timestamp2: uint32(timestamp),
						}
						err := sendTicket(conn, ticket)
						if err != nil {
							log.Printf("sendTicket(%v): %v", ticket, err)
						}
						tickets[plate][day] = true
					}
				}

				return true
			})

		case 0x40: // WantHeartbeat
			if _, ok := seenHeartbeat[conn]; ok {
				return errors.New("already got a WantHeartbeat")
			}
			logger.Printf("got heartbeat: interval: ")
			seenHeartbeat[conn] = struct{}{}
			// The server must now send Heartbeat messages to this client at the given interval, which is specified in "deciseconds", of which there are 10 per second. (So an interval of "25" would mean a Heartbeat message every 2.5 seconds). The heartbeats help to assure the client that the server is still functioning, even in the absence of any other communication.
			rate := time.Duration(be.Uint32(data)) * (time.Second / 10)
			logger.Printf("WantHeartbeat: interval: %s", rate)
			tick := time.NewTicker(rate)
			defer tick.Stop()
			defer delete(seenHeartbeat, conn)
			go func() {
				for range tick.C {
					conn.Write([]byte{0x41}) // Heartbeat
				}
			}()

		case 0x80: // IAmCamera
			_, alreadyDispatcher := dispatchersToRoads[conn]
			_, alreadyCamera := cameras[conn]
			if alreadyCamera || alreadyDispatcher {
				return errors.New("already a dispatcher or camera")
			}
			if len(data) != 6 {
				return fmt.Errorf("IAmCamera; expected length 6, got %d", len(data))
			}
			cam := IAmCamera{Road: be.Uint16(data[:2]), Mile: be.Uint16(data[2:4]), Limit: be.Uint16(data[4:])}
			cameras[conn] = cam
			logger.SetPrefix(fmt.Sprintf("%s %#v\t", logger.Prefix(), cam))
			logger.Printf("got type")
			defer delete(cameras, conn)

		case 0x81: // IAmDispatcher
			_, alreadyDispatcher := dispatchersToRoads[conn]
			_, alreadyCamera := cameras[conn]
			if alreadyCamera || alreadyDispatcher {
				return fmt.Errorf("already a dispatcher or camera")
			}
			n, data := int(data[0]), data[1:]
			if len(data) != (2*n)+1 {
				return fmt.Errorf("IAmDispatcher: got len %d, expected %d", len(data), (2*n)+1)
			}
			roads := make([]uint16, n)
			for i := range roads {
				roads[i] = be.Uint16(data[i*2 : i*2+1])
				if roadsToDispatchers[roads[i]] == nil {
					roadsToDispatchers[roads[i]] = make(set.HSet[*net.TCPConn])
				}
				roadsToDispatchers[roads[i]].Insert(conn)
			}
			defer func() {
				delete(dispatchersToRoads, conn)
				for _, r := range roads {
					roadsToDispatchers[r].Remove(conn)
				}
			}()
		}

	}
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func sendTicket(conn *net.TCPConn, t struct {
	Plate                     string
	Road, Mile1, Mile2, Speed uint16
	Timestamp1, Timestamp2    uint32
}) error {
	var b = make([]byte, 0, 0x1FF)

	if len(t.Plate) > 0xFF {
		panic("plate len too long: must be <= 0xFF")
	}
	// When the server detects that a car's average speed exceeded the speed limit between 2 observations,
	// it generates a Ticket message detailing the number plate of the car (plate), the road number of the cameras (road),
	// the positions of the cameras (mile1, mile2), the timestamps of the observations (timestamp1, timestamp2), and the inferred average speed of the car multiplied by 100, and expressed as an integer (speed).
	if t.Timestamp1 > t.Timestamp2 { /* mile1 and timestamp1 must refer to the earlier of the 2 observations (the smaller timestamp), and mile2 and timestamp2 must refer to the later of the 2 observations (the larger timestamp). */
		t.Timestamp1, t.Timestamp2 = t.Timestamp2, t.Timestamp1
		t.Mile1, t.Mile2 = t.Mile2, t.Mile1
	}
	b = append(b, 0x82) // Ticket
	b = append(b, byte(len(t.Plate)))
	b = append(b, t.Plate...)
	b = be.AppendUint16(b, t.Road)
	b = be.AppendUint16(b, t.Mile1)
	b = be.AppendUint32(b, t.Timestamp1)
	b = be.AppendUint16(b, t.Mile2)
	b = be.AppendUint32(b, t.Timestamp2)
	b = be.AppendUint16(b, 100*t.Speed)
	_, err := conn.Write(b)
	return err
}
