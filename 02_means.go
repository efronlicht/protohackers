package protohackers

import (
	"encoding/binary"
	"io"
	"log"
	"math"
	"net"

	"github.com/tidwall/btree"
)

type Type byte

func Means(conn *net.TCPConn) error {
	req := make([]byte, 9)
	var prices btree.Map[int32, int32]
SERVE:
	for {
		_, err := io.ReadFull(conn, req)
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		switch req[0] {
		default:
			log.Printf("unexpected packet type '%c'", req[0])
			continue SERVE
		case 'I':
			timestamp, val := int32(binary.BigEndian.Uint32(req[1:5])), int32(binary.BigEndian.Uint32(req[5:]))
			prices.Set(timestamp, val)
			log.Println(prices.KeyValues())
		case 'Q':
			start, end := int32(binary.BigEndian.Uint32(req[1:5])), int32(binary.BigEndian.Uint32(req[5:]))
			var i, total int32
			prices.Ascend(start, func(timestamp int32, val int32) bool {
				if timestamp > end {
					return false
				}
				i += 1
				total += val
				return true
			})
			if i == 0 {
				conn.Write(make([]byte, 4))
				continue SERVE
			}
			resp := make([]byte, 4)
			binary.BigEndian.PutUint32(resp, uint32(int32((math.Floor(float64(total) / float64(i))))))
			conn.Write(resp)
		}
	}
}
