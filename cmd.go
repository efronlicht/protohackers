package protohackers

import (
	"fmt"
	"net"
)

func ServeTCP(name string, port int, handle func(*net.TCPConn) error) error {
	listener, err := net.ListenTCP("tcp", &net.TCPAddr{Port: port})
	wrapErr := func(err error) error { return fmt.Errorf("ServeTCP: %s at %d: %w", name, port, err) }
	if err != nil {
		return wrapErr(err)
	}
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			return wrapErr(err)
		}
		if err := handle(conn); err != nil {
			return wrapErr(err)
		}
	}
}
