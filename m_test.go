package protohackers

import (
	"bufio"
	"encoding/binary"
	"io"
	"net"
	"os"
	"sync/atomic"
	"testing"
	"time"
)

var port atomic.Int32

func TestMain(m *testing.M) {

	port.Add(6400)
	os.Exit(m.Run())

}

func TestEcho(t *testing.T) {
	t.Parallel()
	port := int(port.Add(1))
	go ServeTCP(t.Name(), port, Echo)
	time.Sleep(100 * time.Millisecond)
	conn := must(net.DialTCP("tcp", nil, &net.TCPAddr{Port: port}))

	conn.Write([]byte("hi!"))
	conn.CloseWrite()
	time.Sleep(100 * time.Millisecond)
	buf := make([]byte, 4096)
	n := must(conn.Read(buf))
	if got := string(buf[:n]); got != "hi!" {
		t.Fatalf("unexpected result; got %q", got)
	}
	conn.Close()
}
func must[T any](t T, err error) T {
	if err != nil {
		panic(err)
	}
	return t
}
func TestIsPrime(t *testing.T) {
	t.Parallel()
	port := int(port.Add(1))

	go ServeTCP(t.Name(), port, IsPrime)
	time.Sleep(100 * time.Millisecond)
	conn := must(net.DialTCP("tcp", nil, &net.TCPAddr{Port: port}))

	conn.Write(toJSONLine(struct {
		Method string
		Number int
	}{"isPrime", 5}))
	conn.Write([]byte("malformed\n"))
	conn.CloseWrite()
	time.Sleep(100 * time.Millisecond)
	scanner := bufio.NewScanner(conn)
	scanner.Scan()
	resp := must(fromJSON[struct{ Prime bool }](scanner.Bytes()))
	if !resp.Prime {
		t.Fatalf("expected prime, but got %v", resp.Prime)
	}
}

func TestMeans(t *testing.T) {
	t.Parallel()
	port := int(port.Add(1))
	go ServeTCP(t.Name(), port, Means)
	time.Sleep(100 * time.Millisecond)

	conn, err := net.DialTCP("tcp", nil, &net.TCPAddr{Port: port})
	if err != nil {
		panic(err)
	}

	Q := func(start, end int32) {
		b := []byte{'Q'}
		b = binary.BigEndian.AppendUint32(b, uint32(start))
		conn.Write(binary.BigEndian.AppendUint32(b, uint32(end)))
		time.Sleep(20 * time.Millisecond)
	}
	I := func(timestamp, val int32) {
		b := []byte{'I'}
		b = binary.BigEndian.AppendUint32(b, uint32(timestamp))
		conn.Write(binary.BigEndian.AppendUint32(b, uint32(val)))
		time.Sleep(20 * time.Millisecond)
	}
	assertGot := func(want int32) {
		b := make([]byte, 4)
		must(io.ReadFull(conn, b))
		if got := int32(binary.BigEndian.Uint32(b)); got != want {
			t.Fatalf("expected %d, but got %d", got, want)
		}

	}
	Q(0, -1)
	assertGot(0)
	I(1, 10)
	I(2, 20)
	Q(1, 3)
	assertGot(15)

}
