package protohackers

import (
	"bytes"
	"strings"
)

const backslashReplacement = "🔥"
const slashReplacement = "🌈"

func escape(s string) string {
	s = strings.ReplaceAll(s, `\\`, backslashReplacement)
	s = strings.ReplaceAll(s, `\/`, slashReplacement)
	return s
}
func unescape(s string) string {
	s = strings.ReplaceAll(s, backslashReplacement, `\`)
	s = strings.ReplaceAll(s, slashReplacement, `/`)
	return s
}

func reverseLines(in <-chan []byte, out chan<- []byte) {
	b := make([]byte, 0, 10000)
	for packet := range in {
		b = append(b, packet...)
		i := bytes.IndexByte(b, '\n')
		if i == -1 {
			continue // no newline yet
		}
		for j, k := 0, i-1; j < k; j, k = j+1, k-1 {
			b[j], b[k] = b[k], b[j]
		}
		out <- b[:i]
		b = b[i+1:]
	}

}

const ENABLE_LOG = false

/*
func decode(conn *net.UDPConn, retransmissionTimeout, expirationTimeout time.Duration) error {
	buf := make([]byte, 1024)
	type metadata struct {
		received, sent, acknowledged int
	}
	var mux sync.Mutex
	var sessions = make(map[int]metadata)

LOOP:
	for {
		n, addr, err := conn.ReadFrom(buf)

		if err != nil {
			return fmt.Errorf("udp read: %w", err)
		}
		packet := escape(string(buf[:n]))
		if len(packet) == 0 || packet[0] != '/' {
			log.Printf("invalid packet: must start with /")
			continue LOOP
		}
		packet = packet[1:]
		fields := strings.Split(packet, "/")
		if len(fields) < 2 {
			log.Printf("invalid packet: must have at least 2 fields")
			continue LOOP
		}
		type_ := fields[0]

		id, err := strconv.Atoi(fields[1])
		if err != nil {
			log.Printf("invalid packet: sessionID (field 1) must be an integer: %v", err)
		}
		sendf := func(format string, args ...interface{}) {
			s := fmt.Sprintf(format, args...)
			if ENABLE_LOG {
				log.Printf("session %d: type %s: addr: %s: sending: %s", id, type_, addr, s)
			}
			_, err := conn.Write([]byte(s))
			if err != nil {
				log.Printf("writing UDP packet: %v", err)
			}
		}
		switch type_ {
		case "close":
			sendf("/close/%d/", id)
		case "ack":
			meta, ok := sessions[id]
			if !ok { // misbehaving client: close connection
				sendf("/close/%d/", id)
				continue LOOP
			}
			ack := int(meta.acknowledged.Load())
			sent := int(meta.sent.Load())

			switch pos, err := strconv.Atoi(fields[2]); {
			case err != nil:
				log.Printf("invalid packet: pos (field 2) must be an integer: %v", err)
			case pos > ack: // duplicate ack: ignore
			case pos < sent:
				panic("TODO: retransmit missing packets")
			case pos > sent: // misbehaving client: close connection
				sendf("/close/%d/", id)
				continue LOOP
			case pos == sent: // regular ack

			case pos == sent[id]: // duplicate ack: ignore
			case pos < sent[id]:
				// retransmit missing packets

			}
		case "connect":
			sendf("/ack/%d/0", id)
			sessions[id] = new(metadata)
		case "data":
			expectPos, err := strconv.Atoi(fields[2])
			if err != nil {
				log.Printf("invalid packet: pos (field 2) must be an integer: %v", err)
				continue LOOP
			}
			currentPos, ok := received[id]
			if !ok {
				sendf("/close/%d/", id)
				continue LOOP
			}
			if currentPos != expectPos { // missed a packet: acknowledge last packet received
				sendf("/ack/%d/%d", id, currentPos)
				continue LOOP
			}
			data := unescape(fields[3])
			received[id] += len(data)
			sendf("/ack/%d/%d", id, received[id])
		}
	}
}
*/
