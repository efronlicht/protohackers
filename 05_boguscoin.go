package protohackers

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"regexp"
)

// see https://regex101.com/r/8WHjlS/1
const reFormat = `(^| )` + // at start or prefixed w/ space
	`(?:7(?:[a-zA-Z0-9]){25,34})` + // it starts with a "7" and  consists of at least 26, and at most 35, alphanumeric characters
	`($| )` // at end or followed by space.

var bogusRE = regexp.MustCompile(reFormat)

var rewritePattern = []byte("$1" + "7YWHMfk9JZe0LM0g1ZauHuiSxhI" + "$2")

func ManInTheMiddle(client *net.TCPConn) error {
	var serverIP net.IP
	{ // resolve server IP
		ips, err := net.LookupIP("chat.protohackers.com")
		if err != nil {
			return err
		}
		if len(ips) == 0 {
			return errors.New("no IP found")
		}
		serverIP = ips[0]
	}
	server, err := net.DialTCP("tcp", nil, &net.TCPAddr{IP: serverIP, Port: 16963})
	if err != nil {
		return err
	}
	defer server.Close()
	defer client.Close()
	rewrite := func(src, dst *net.TCPConn) {
		for scanner := bufio.NewScanner(src); scanner.Scan(); {
			b := bogusRE.ReplaceAll(scanner.Bytes(), rewritePattern)
			fmt.Fprintf(dst, "%s\n", b)
		}
	}
	go rewrite(client, server)
	rewrite(server, client)
	return nil
}
