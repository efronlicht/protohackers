package protohackers

import (
	"net"
	"strings"
)

func BasicDB04(port int) error {
	db := map[string]string{"version": "-3.14159"}
	conn, err := net.ListenUDP("udp", &net.UDPAddr{Port: port})
	if err != nil {
		return err
	}
	buf := make([]byte, 999)
	for {
		n, addr, err := conn.ReadFrom(buf)
		if err != nil {
			return err
		}
		s := string(buf[:n])
		if k, v, ok := strings.Cut(s, "="); ok && s != "version" {
			db[k] = v
		} else if v, ok := db[s]; ok {
			conn.WriteTo([]byte(v), addr)
		}
	}
}
