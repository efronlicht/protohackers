package protohackers

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"sync"
	"sync/atomic"
)

type Job struct {
	Priority int
	ID       int64
	Val      json.RawMessage
}

var ids atomic.Int64

func nextID() int64 { return ids.Add(1) }

var queues = make(map[string]map[int64]Job) // queue_name => id => Job
var mux sync.Mutex

const responseNoJob = `{"status": "no job"}` + "\n"

func handle09(conn *net.TCPConn) error {

	// each request and response is a single string, terminated by a newline, that's a JSON object
	sendErrf := func(format string, args ...any) {
		msg := fmt.Sprintf(format, args...)
		log.Print(msg)
		fmt.Fprintf(conn, `{"status": "error", "error": %q}`, msg)
	}
	scanner := bufio.NewScanner(conn)
	jobs := make(map[int64]string) // ID to Queue
	defer func() {                 // on exit, abort all the jobs this client was working on
		mux.Lock()
		for id, queue := range jobs {
			delete(queues[queue], id)
		}
		mux.Unlock()
	}()
READLINE:

	for scanner.Scan() {
		request, err := fromJSON[struct {
			Request string   `json:"request"` // "put", "get", "delete", "abort"
			Queues  []string `json:"queues"`  // GET only
			Wait    bool     `json:"wait"`    // GET only

			Queue string          `json:"queue"` // PUT only
			Job   json.RawMessage `json:"job"`   // PUT only
			Pri   int             `json:"pri"`   // PUT only

			ID int64 `json:"id"` // DELETE, ABORT only
		}](scanner.Bytes())
		if err != nil {
			sendErrf("invalid request: %v", err)
			continue READLINE
		}
		switch request.Request {
		default:
			sendErrf("invalid request: unknown request type %q", request.Request)
			continue READLINE
		case "put":
			if request.Queue == "" || request.Job == nil || request.Pri < 0 {
				sendErrf("put: missing one or more of queue, job, or pri")
				continue READLINE
			}
			if len(request.Queues) != 0 || request.Wait || request.ID != 0 {
				sendErrf("put: extra fields")
				continue READLINE
			}
			id := nextID()

			mux.Lock()
			if queues[request.Queue] == nil {
				queues[request.Queue] = make(map[int64]Job)
			}
			queues[request.Queue][id] = Job{Priority: request.Pri, ID: id, Val: request.Job}
			mux.Unlock()
			jobs[id] = request.Queue
			if _, err := fmt.Fprintf(conn, `{"status": "ok", "id":%d}`+"\n", id); err != nil {
				return fmt.Errorf("put: %w", err)
			}

		case "get":
			if request.Queue != "" || request.Job != nil || request.Pri != 0 {
				sendErrf("get: extra fields")
				continue READLINE
			}
			if len(request.Queues) == 0 {
				// TODO: send error response: missing fields
				continue READLINE
			}
			// TODO: if we have wait and there's no queues, do we just block forever?
			// we want the job with the HIGHEST priority in any of the queues
			var maxJobQueue string
			var maxJobID int64
			var maxJobPriority = -1
			mux.Lock()
			for maxJobPriority == -1 && request.Wait {
				for _, k := range request.Queues {
					for _, j := range queues[k] {
						if j.Priority > maxJobPriority {
							maxJobPriority = j.Priority
							maxJobQueue = k
							maxJobID = j.ID
						}
					}
				}
			}
			mux.Unlock()
			if maxJobPriority == -1 {
				if _, err := conn.Write([]byte(responseNoJob)); err != nil {
					return fmt.Errorf("get: %w", err) // client disconnected
				}
				continue READLINE
			}

			// we found a job
			resp := toJSONLine(struct {
				Status string          `json:"status"`
				ID     int64           `json:"id"`
				Job    json.RawMessage `json:"job"`
				Pri    int             `json:"pri"`
				Queue  string          `json:"queue"`
			}{
				Status: "ok",
				ID:     maxJobID,
				Job:    queues[maxJobQueue][maxJobID].Val,
				Pri:    maxJobPriority,
				Queue:  maxJobQueue,
			})
			if _, err := conn.Write(resp); err != nil {
				return fmt.Errorf("get: %w", err) // client disconnected
			}

		case "delete":
			if request.ID <= 0 { // can't be zero, since the first call to nextID() returns 1
				sendErrf("delete: bad id")
				continue READLINE
			}
			k, ok := jobs[request.ID]
			if !ok {
				log.Printf("delete: id %d not found", request.ID)
				conn.Write([]byte(responseNoJob))
			}
			mux.Lock()
			delete(queues[k], request.ID)
			mux.Unlock()
			conn.Write([]byte(`{"status": "ok"}` + "\n"))
		case "abort":

		}

	}
	return nil

}

func swapRemove[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]  // copy last element to index i
	return s[:len(s)-1] // truncate slice
}
