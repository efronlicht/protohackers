package protohackers

import (
	"errors"
	"fmt"
)

type CipherKey byte

func swapBits(i int, b byte) byte {
	var swapped byte
	for j := byte(0); j < 8; j++ {
		if b&1<<(7-j) != 0 {
			swapped |= 1 << j
		}
	}
	return swapped
}

var offsets = [6]int{
	0x00: 1,
	0x01: 1,
	0x02: 2,
	0x03: 1,
	0x04: 2,
	0x05: 1,
}

func compileCipher(cipher []byte) (compiled func(i int, b byte) byte, err error) {
	var funcs []func(i int, b byte) byte

	for j := 0; j < len(cipher); j += offsets[cipher[j]] {
		key := cipher[j]
		if j == len(cipher)-1 && key != 0x00 {
			return nil, errors.New("endOfCipher must be last key")
		}
		switch key {
		case 0x00: // EndOfCipher
			if j != len(cipher)-1 {
				return nil, errors.New("cipher must end with EndOfCipher")
			}
		case 0x01: // reversebits
			funcs = append(funcs, swapBits)
		case 0x02: // XOR N
			n := cipher[j+1]
			funcs = append(funcs, func(_ int, b byte) byte { return b ^ n })
		case 0x03: // XOR POS
			funcs = append(funcs, func(i int, b byte) byte { return b ^ byte(i) })
		case 0x04: // add N
			n := cipher[j+1]
			funcs = append(funcs, func(_ int, b byte) byte { return b + n })
		case 0x05: // add POS
			funcs = append(funcs, func(i int, b byte) byte { return b + byte(i) })
		default:
			return nil, fmt.Errorf("unknown cipher key: expected 0x00-0x05, got %#02X", key)
		}
	}
	// a better solution would pre-process the cipher to cut down on the complexity.
	// a few possible optimizations:
	// merge addN(a), AddN(b) into AddN(a+b%0xFF)
	// merge reversebits, reversebits into nops
	compiled = func(i int, b byte) byte {
		for _, f := range funcs {
			b = f(i, b)
		}
		return b
	}

	for i := 0; i < 0xFF; i++ {
		for b := byte(0); b < 0xFF; b++ {
			if compiled(i, b) != b {
				return compiled, nil
			}
		}
	}

	return compiled, errors.New("cipher is a nop")
}

func apply(cipher []byte, f func(i int, b byte) byte) {
	for i, b := range cipher {
		cipher[i] = f(i, b)
	}
}
